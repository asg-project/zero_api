package com.flying.Dutchman.service.handler;

import java.util.List;
import java.util.Optional;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.flying.Dutchman.service.accRepository;
import com.flying.Dutchman.service.model.AccModel;



@Controller
@RequestMapping("/account")
public class accHandler {
	
	@Autowired
	accRepository repo;
	
	@CrossOrigin()
	@RequestMapping(value="/generate", method=RequestMethod.GET)
	public ResponseEntity<List<AccModel>> getAllAccs(){
		
		List<AccModel> listObjects = repo.findAll();
		AccModel accobject = new AccModel();

		for(int i = 0; i != listObjects.size(); i++) {
			accobject = listObjects.get(i);
			
			
			accobject.setMetalAmmount(accobject.getMetalAmmount() + (10 + accobject.getLevelMetalMine() ));
			accobject.setCrystalAmmount(accobject.getCrystalAmmount() + (10 + accobject.getLevelCrystalMine() ));

			repo.save(accobject);
			
		}
		
		
		
		return new ResponseEntity<List<AccModel>>(listObjects,HttpStatus.OK);
	
	}
	


}

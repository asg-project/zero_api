package com.flying.Dutchman.service;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.flying.Dutchman.service.model.AccModel;


public interface accRepository extends MongoRepository<AccModel,String>{
	
	
	AccModel findByGoogleID (String googleId);
	
	Optional<AccModel> findByGoogleIDO (String googleId);
	
}

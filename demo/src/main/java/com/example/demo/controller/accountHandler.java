package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.AccountModel;
import com.example.demo.service.accountRepository;



@Controller
@RequestMapping("/account")
public class accountHandler {
	
	@Autowired
	accountRepository repo;
	
	@CrossOrigin()
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<List<AccountModel>> getAllAccs(){
		
		List<AccountModel> listObjects = repo.findAll();
		
		
		return new ResponseEntity<List<AccountModel>>(listObjects,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/get", method=RequestMethod.GET)
	public ResponseEntity<?> getOneAcc(@RequestParam(value="idGoogle", defaultValue="0") String idItem){
		
	AccountModel object;
		
		object = repo.findByGoogleID(idItem);
		
		
		return new ResponseEntity<AccountModel>(object,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ResponseEntity<?> addAcc(@RequestBody(required=false) AccountModel NewAccountModel){
		
		repo.save(NewAccountModel);
		
		return new ResponseEntity<AccountModel>(NewAccountModel,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public ResponseEntity<?> deleteOneItens(@RequestParam(value="idGoogle", defaultValue="0") String idItem){
		
	
		AccountModel object = repo.findByGoogleID(idItem);
		
		
		//repo.delete(object.get());
		
		return new ResponseEntity<AccountModel>(object,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/start", method=RequestMethod.GET)
	public ResponseEntity<?>  startGame(@RequestParam(value="idGoogle", defaultValue="0") String idGoogle
			){
		
		AccountModel object = repo.findByGoogleID(idGoogle);
		
	
		Optional <AccountModel> optionalobject = repo.findByGoogleIDO(idGoogle);
		
		
	
		if(optionalobject.isPresent()){
		
			return home(optionalobject.get().getGoogleID());

		}else{
			return novaAcc(idGoogle);			

		}

	
	}
	
	@CrossOrigin()
public ResponseEntity<?> home(@RequestParam(value="idGoogle", defaultValue="0") String idItem){
	

	AccountModel object = repo.findByGoogleID(idItem);
	

	
	
	return new ResponseEntity<AccountModel>(object,HttpStatus.OK);

}
	@CrossOrigin()
public ResponseEntity<?> novaAcc(@RequestParam(value="idGoogle", defaultValue="0") String idItem){
	

	AccountModel object = new AccountModel();
	
	object.setPlanetTemperature(23);
	object.setCoordenates("00-00-00");
	object.setPlanetName("Planet Zero");
	object.setPlanetTemperature(234);
	
	
	object.setMetalAmmount(100);
	object.setCrystalAmmount(100);
	object.setEnergyAmmount(0);
	
	
	
	object.setGoogleID(idItem);
	object.setGoogleIDO(idItem);
	
	
	
	
	object.setLevelCrystalMine(0);
	object.setLevelEnergyPlant(0);
	object.setLevelMetalMine(0);
	object.setLevelMetalStorage(0);
	object.setLevelMisileSilo(0);
	object.setLevelpaceDock(0);
	object.setLevelRoboticsFactory(0);
	object.setLevelShipyard(0);
	
	repo.save(object);
	
	return new ResponseEntity<AccountModel>(object,HttpStatus.OK);

}
	



}

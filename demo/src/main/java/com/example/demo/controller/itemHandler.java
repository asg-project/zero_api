package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.ItemObject;
import com.example.demo.service.itemRepository;


@Controller
@RequestMapping("/item")
public class itemHandler {
	
	@Autowired
	itemRepository repo;
	
	@CrossOrigin()
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<List<ItemObject>> getAllItens(){
		
		List<ItemObject> listObjects = repo.findAll();
		
		
		return new ResponseEntity<List<ItemObject>>(listObjects,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/get", method=RequestMethod.GET)
	public ResponseEntity<?> getOneItens(@RequestParam(value="idItem", defaultValue="0") String idItem){
		
		ItemObject object = new ItemObject();
		
		object = repo.findByIdItem(idItem);
		
		
		return new ResponseEntity<ItemObject>(object,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ResponseEntity<?> getOneItens(@RequestBody(required=false) ItemObject NewItemObject){
		
		repo.save(NewItemObject);
		
		return new ResponseEntity<ItemObject>(NewItemObject,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public ResponseEntity<?> deleteOneItens(@RequestParam(value="idItem", defaultValue="0") String idItem){
		
	
		ItemObject object = repo.findByIdItem(idItem);
		
		
		repo.delete(object);
		
		return new ResponseEntity<ItemObject>(object,HttpStatus.OK);
	
	}
	
	@CrossOrigin()
	@RequestMapping(value="/edit", method=RequestMethod.POST)
	public ResponseEntity<?> editOneItem(@RequestParam(value="idItem", defaultValue="0") String idItem,
												@RequestBody(required=false) ItemObject EditedItemObject
			){
		
	
		
		ItemObject object = repo.findByIdItem(idItem);
		
		object.setName(EditedItemObject.getName());
		object.setDescription(EditedItemObject.getDescription());
		object.setQuant(EditedItemObject.getQuant());
		
		
		repo.save(object);
		
		return new ResponseEntity<ItemObject>(object,HttpStatus.OK);
	
	}
	

}

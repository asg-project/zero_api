package com.example.demo.service;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.AccountModel;

public interface accountRepository extends MongoRepository<AccountModel,String>{
	
	
	AccountModel findByGoogleID (String googleId);
	
	Optional<AccountModel> findByGoogleIDO (String googleId);
	
}
